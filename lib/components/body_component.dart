import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/entries_bloc.dart';

class BodyComponent extends StatelessWidget {

  const BodyComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EntriesBloc, EntriesState>(
        builder: ((context, state) {
          if (state is EntriesInitial) {
            return const Center(
              child: CircularProgressIndicator(color: Colors.blueGrey),
            );
          }
          
          if (state is EntriesLoaded) {
            return ListView.builder(
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text(state.entries[index].title),
                  trailing: Text(state.entries[index].mood.value),
                );
              },
              itemCount: state.entries.length,
            );
          }

          return const Text('Something went wrong');
        }),
      );
  }
}
