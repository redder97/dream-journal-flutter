import 'package:dream_journal/bloc/entries_bloc.dart';
import 'package:dream_journal/models/entry_model.dart';
import 'package:dream_journal/models/mood_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CreateComponent extends StatefulWidget {
  const CreateComponent({Key? key}) : super(key: key);

  @override
  _CreateComponentState createState() => _CreateComponentState();
}

class _CreateComponentState extends State<CreateComponent> {
  final _formKey = GlobalKey<FormState>();

  final titleController = TextEditingController();
  final contentController = TextEditingController();

  late Entry currentEntry;
  String id = '';
  String currentTitle = '';
  String currentContent = '';
  DateTime currentDateTimeCreated = DateTime.now();
  Mood? currentMood = Mood.ANGRY;

  @override
  Widget build(BuildContext context) {
    return BlocListener<EntriesBloc, EntriesState>(
      listener: (context, state) {
        if (state is EntriesLoaded) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text(
                'Dream entry has been added!',
              ),
            ),
          );
        }
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(bottom: 10),
                child: TextFormField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter a title';
                    }

                    return null;
                  },
                  textAlign: TextAlign.center,
                  decoration: const InputDecoration(
                    labelText: 'Title',
                    border: OutlineInputBorder(),
                  ),
                  controller: titleController,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 10),
                child: TextFormField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter a text body';
                    }

                    return null;
                  },
                  decoration: const InputDecoration(
                    labelText: 'Tell me about your dream',
                    border: OutlineInputBorder(),
                  ),
                  cursorHeight: 4,
                  maxLines: 5,
                  controller: contentController,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 10),
                child: DropdownButtonFormField(
                  value: currentMood,
                  items: Mood.all.map((mood) {
                    return DropdownMenuItem(
                        value: mood, child: Text(mood.value));
                  }).toList(),
                  onChanged: ((value) {
                    setState(() {
                      currentMood = value;
                    });
                  }),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 10),
                child: ElevatedButton(
                  onPressed: () {
                    currentEntry = Entry(
                      id: id,
                      title: titleController.text,
                      content: contentController.text,
                      dateTimeCreated: currentDateTimeCreated,
                      mood: currentMood!,
                    );

                    context.read<EntriesBloc>().add(AddEntry(currentEntry));
                  },
                  child: const Text('Submit'),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
