enum Mood {

  HAPPY('Happy'), SAD('Sad'), ANGRY('Angry'), RELIEVED('Relieved'), Scared('Scared');

  const Mood(this.value);

  final String value;

  static List<Mood> get all => Mood.values;
}