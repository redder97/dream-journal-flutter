import 'package:dream_journal/models/mood_model.dart';
import 'package:equatable/equatable.dart';

class Entry extends Equatable{
  final String id;
  final String title;
  final String content;
  final DateTime dateTimeCreated;
  final Mood mood;

  const Entry({
    required this.id,
    required this.title,
    required this.content,
    required this.dateTimeCreated,
    required this.mood,
  });

  @override
  List<Object?> get props => [id, title, content, dateTimeCreated, mood];

  
}