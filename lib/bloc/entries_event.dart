part of 'entries_bloc.dart';

abstract class EntriesEvent extends Equatable {
  const EntriesEvent();

  @override
  List<Object> get props => [];
}

class LoadEntries extends EntriesEvent {}

class AddEntry extends EntriesEvent {
  final Entry entry;

  const AddEntry(this.entry);

  @override
  List<Object> get props => [entry];
}

class RemoveEntry extends EntriesEvent {
  final Entry entry;

  const RemoveEntry(this.entry);

  @override
  List<Object> get props => [entry];
}



