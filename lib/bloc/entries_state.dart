part of 'entries_bloc.dart';

abstract class EntriesState extends Equatable {
  const EntriesState();
  
  @override
  List<Object> get props => [];
}

class EntriesInitial extends EntriesState {}

class EntriesLoaded extends EntriesState {
  final List<Entry> entries;

  const EntriesLoaded({
    required this.entries
  });

  @override
  List<Object> get props => [entries];


  static List<Entry> get mockEntries => [
    Entry(
      id: '1',
      title: 'This is an entry',
      content: 'Cool Things',
      dateTimeCreated: DateTime.now(),
      mood: Mood.HAPPY
    ),

    Entry(
      id: '2',
      title: 'Today was a great day',
      content: 'Cool Things again',
      dateTimeCreated: DateTime.now(),
      mood: Mood.ANGRY
    ),

  ];
}