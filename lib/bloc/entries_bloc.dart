import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../models/entry_model.dart';
import '../models/mood_model.dart';

part 'entries_event.dart';
part 'entries_state.dart';

class EntriesBloc extends Bloc<EntriesEvent, EntriesState> {
  EntriesBloc() : super(EntriesInitial()) {
    on<LoadEntries>((event, emit) async {
      await Future<void>.delayed(const Duration(seconds: 1));
      emit(EntriesLoaded(entries: EntriesLoaded.mockEntries));
    });

    on<AddEntry>((event, emit) async {
      if (state is EntriesLoaded) {
        final state = this.state as EntriesLoaded;
        emit(
            EntriesLoaded(entries: List.from(state.entries)..add(event.entry)));
      }
    });

    on<RemoveEntry>((event, emit) async {
      if (state is EntriesLoaded) {
        final state = this.state as EntriesLoaded;
        emit(EntriesLoaded(
            entries: List.from(state.entries)..remove(event.entry)));
      }
    });
  }
}
