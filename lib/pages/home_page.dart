import 'package:dream_journal/bloc/entries_bloc.dart';
import 'package:dream_journal/components/body_component.dart';
import 'package:dream_journal/components/create_component.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  int currentPage = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
      ),
      body: [
        const CreateComponent(),
        const BodyComponent(),
      ][currentPage],
      bottomNavigationBar: NavigationBar(
        destinations: const [
          NavigationDestination(
            icon: Icon(Icons.edit),
            label: 'New',
          ),
          NavigationDestination(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
        ],
        onDestinationSelected: (index) {
          setState(() {
            currentPage = index;
          });
        },
        selectedIndex: currentPage,
      ),
    );
  }
}
